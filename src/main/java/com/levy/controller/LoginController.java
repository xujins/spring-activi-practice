package com.levy.controller;


import com.levy.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;


@Controller
public class LoginController {

	@Autowired
	LoginService loginService;

	@RequestMapping(value = "/loginalidate",method = RequestMethod.POST)
	public String loginvalidate(@RequestParam("username") String username, @RequestParam("password") String pwd, HttpSession httpSession){
		if(username==null) {
			return "login";
		}

		String realpwd=loginService.getpwdbyname(username);
		if(realpwd!=null&&pwd.equals(realpwd))
		{
			httpSession.setAttribute("username", username);
			return "/index.html";
		}else {
			return "/fail.html";
		}
	}




//	@RequestMapping("/logout")
//	public String logout(HttpSession httpSession){
//		httpSession.removeAttribute("username");
//		return "login";
//	}
  }
